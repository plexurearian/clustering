from module import *
import sys, getopt

def main(argv):
	inputfile = ''
	outputfolder = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
	except getopt.GetoptError:
		print('main.py -i <inputfile> -o <outputfolder>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('main.py -i <inputfile> -o <outputfolder>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-o", "--ofile"):
			outputfolder = arg
	Labeled_data_path = Cluster_data(inputfile,outputfolder)
	Ac = classification_model(Labeled_data_path,outputfolder)
	print("Classification Accuracy: ", Ac)
	anomaly_detction(inputfile,outputfolder)

if __name__ == '__main__':
    main(sys.argv[1:])