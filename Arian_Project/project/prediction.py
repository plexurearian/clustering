from module import *
import sys, getopt

def main(argv):
	inputfile = ''
	outputfolder = ''
	modelfile = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:m:",["ifile=","ofile=","mfile="])
	except getopt.GetoptError:
		print('prediction.py -i <inputfile> -o <outputfolder> -m <modelfile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('prediction.py -i <inputfile> -o <outputfolder> -m <modelfile>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-o", "--ofile"):
			outputfolder = arg
		elif opt in ("-m", "--mfile"):
			modelfile = arg
	predict_label(inputfile,outputfolder,modelfile)
	
if __name__ == '__main__':
	print(sys.argv[1:])
	main(sys.argv[1:])
