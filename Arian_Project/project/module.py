import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import IsolationForest
from sklearn.manifold import TSNE
from matplotlib import pyplot as plt
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import accuracy_score
import math
import pickle
import os

def Cluster_data(data_path,output_folder,base_feature = 'all'):

    data = pd.read_csv(data_path, encoding='latin_1', low_memory=False)
    headers_name = list(data)
    df_values = data.values
    header_string_df = list(data.select_dtypes(exclude=np.number))
    if len(header_string_df)!=0:
        for i in range(len(header_string_df)):
            df_values[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]] = pd.factorize(df_values[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]])[0]
    if base_feature == 'all':
        kmeans = KMeans(n_clusters=2, random_state=0).fit(df_values)
    else:
        IDX = []
        for i in range(len(base_feature)):
            IDX.append(np.where(np.array(headers_name)==base_feature[i])[0][0])
        if len(IDX)>=2:
            kmeans = KMeans(n_clusters=2, random_state=0).fit(df_values[:,IDX])
        elif len(IDX)==1:
            kmeans = KMeans(n_clusters=2, random_state=0).fit(np.array(df_values[:, IDX]))
    df_class_1 = data.iloc[np.where(kmeans.labels_==0)[0],:]
    df_class_2 = data.iloc[np.where(kmeans.labels_==1)[0],:]
    df_labeled = data.copy()
    df_labeled['Label'] = kmeans.labels_

    df_class_1.to_csv(output_folder+"\\Class_1.csv", encoding='utf-8', index=False)
    df_class_2.to_csv(output_folder+"\\Class_2.csv", encoding='utf-8', index=False)
    df_labeled.to_csv(output_folder+"\\Labeled.csv", encoding='utf-8', index=False)
    return output_folder+"\\Labeled.csv"


def classification_model(data_path,output_folder,train_percentage = 0.8):
    data = pd.read_csv(data_path, encoding='latin_1', low_memory=False)
    data_copy = data.copy()
    data = data_copy.drop('Label', axis=1)
    headers_name = list(data)
    Features = data.values
    header_string_df = list(data.select_dtypes(exclude=np.number))
    if len(header_string_df)!=0:
        for i in range(len(header_string_df)):
            Features[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]] = pd.factorize(Features[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]])[0]

    Label = data_copy['Label']
    Label = Label.values

    X_train = Features[:math.floor(train_percentage*data.shape[0]), :]
    Y_train = Label[:math.floor(train_percentage*data.shape[0])]

    X_test = Features[math.floor(train_percentage*data.shape[0])+1:, :]
    Y_test = Label[math.floor(train_percentage*data.shape[0])+1:]

    clf = AdaBoostClassifier(n_estimators=100, random_state=0)
    clf.fit(X_train, Y_train)

    Y_pred = clf.predict(X_test)
    Accuracy = accuracy_score(Y_test, Y_pred)
    # save the model to disk
    filename = output_folder+"\\finalized_model.sav"
    pickle.dump(clf, open(filename, 'wb'))
    return 100*Accuracy


def anomaly_detction(data_path,output_folder,Personal_ID = 'Personal ID'):
    data = pd.read_csv(data_path, encoding='latin_1', low_memory=False)
    headers_name = list(data)
    df_values = data.values
    header_string_df = list(data.select_dtypes(exclude=np.number))
    if len(header_string_df)!=0:
        for i in range(len(header_string_df)):
            df_values[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]] = pd.factorize(df_values[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]])[0]
    clf=IsolationForest(n_estimators=100,  max_samples='auto', contamination=float(.12), random_state=42, verbose=0)
    clf.fit(df_values)
    pred = clf.predict(df_values)
    data['anomaly']=pred
    outliers = data.loc[data['anomaly'] == -1]
    outlier_index = list(outliers.index)
    IDX_persoanl_id = np.where(np.array(list(data))== Personal_ID)[0][0]
    idxs = list(range(len(headers_name)))
    idxs.pop(IDX_persoanl_id)
    X_embedded = TSNE(n_components=2).fit_transform(df_values[:, idxs])
    normal_data = plt.scatter(X_embedded[:,0], X_embedded[:,1],label='normal')
    plt.hold
    abnormal_data = plt.scatter(X_embedded[outlier_index, 0], X_embedded[outlier_index, 1], label='abnormal')
    plt.legend(handles=[normal_data, abnormal_data])
    plt.show()
    data.to_csv(output_folder+"\\anomaly.csv", encoding='utf-8', index=False)
    return 0
	
def predict_label(data_path,outputfolder,modelfile):
	
	data = pd.read_csv(data_path, encoding='latin_1', low_memory=False)
	headers_name = list(data)
	Features = data.values
	header_string_df = list(data.select_dtypes(exclude=np.number))
	if len(header_string_df)!=0:
		for i in range(len(header_string_df)):
			Features[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]] = pd.factorize(Features[:,np.where(np.array(headers_name)==header_string_df[i])[0][0]])[0]


	X_test = Features
	model = pickle.load(open(modelfile, 'rb'))
	Y_pred = model.predict(X_test)
	data['predicted_Label']=Y_pred
	data.to_csv(outputfolder+"\\Prediction_Results.csv", encoding='utf-8', index=False)
	return 0
