from distutils.core import setup

setup(
    name='ArianProject',
    version='1.0',
    author='Seyed Reza Mir Alavi',
    author_email='aryan.miralavi@gmail.com',
    packages=['project'],
    url='https://bitbucket.org/plexurearian/clustering/src/master/Arian_Project/',
    license='LICENSE.txt',
    description='Clustering and Anaomaly detection.',
    long_description=open('README.txt').read(),
    install_requires=[
        "pandas",
		"numpy",
		"scikit-learn"
		"mathematics",
		"matplotlib",
    ],
)