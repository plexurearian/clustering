
What Is This?
-------------

This module is intended to cluster, classify and detect anomaly over a given
data that is extracted from live CRM/ Marketing platform comprising of anonymous
10K records. This feature/Training set is sampled from a much larger training
set for Machine Learning model training. Data extract comprises of 3 main
categories of features,
Account Features
Personal ID Features
Redemption Activity 
The clustering functionality is aimed to cluster the consumer group into two 
clusters, namely good and bad consumers, in an unsupervised manner. 
Since it is assumed that there is no prior label on the data, the clustering
result will be considered as an initial guess on the labels. Given the extracted
labels from the clustering algorithm, the classification function will create a
model to be used for later prediction.
The anomaly detection function aims to find any possible outliers which may be
considered as fraud transactions via consumers. The population of normal and 
abnormal transactions will be visualised in a 2D plot.


How To Use The Module
-----------------------

To use the module you need to first install the requirements by the following
command at the Arian/dist/ folder: pip install ArianProject-1.0.tar.gz

After installing the requirements, to clsuter, create classification model, 
detect anomaly and visuale the outliers you need to run the main.py as follows:
python main.py -i <inputfile> -o <outputfolder>

To do predict using the existing model you need to run the following command:
python prediction.py -i <inputfile> -o <outputfolder> -m <modelfile>


Output files
-----------------------
The output of clustering will include three files:
1. Class1.csv which includes the consumers belong to class 1.
2. Class2.csv which includes the consumers belong to class 2.
3  Labeled.csv which includes the labels that are associated to all cosumers.

The output of classification will include one file and an accuracy value:
1. finalized_model.sav will include the model that is trained via the labels
extracted from clustering. 
The accuracy will demonstrates the percenatge of accuracy of classification 
model on correct classifiying 20 percent of unseen records.

The output of anomaly detection will include a file and a 2D plot:
1. anomaly.csv will include the anomaly label of the records. 1 means normal
and -1 means abnormal.
The 2D plot visualised the distribution of the features for normal and abnormal
records.

The output of prediction will include one file:
1. Prediction_Results.csv which includes the predicted label for the unseen records.

